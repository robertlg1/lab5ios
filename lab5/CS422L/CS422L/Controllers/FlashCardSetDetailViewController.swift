//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import CoreData
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    var cards: [FlashCardEntity] = []
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    var count = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longPressedGesture)
        makeItPretty()
        dataset()
        tableView.reloadData()
        
    }
    
    func cntxt() -> NSManagedObjectContext {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return context
    }
    
    func dataset() -> [FlashCardEntity]{
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FlashCardEntity")
        var data: [FlashCardEntity] = []
        do{
            data = try context.fetch(fetchRequest) as! [FlashCardEntity]
        }
        catch{
            print(error)
        }
        cards = data
        do {
            try context.save()
        }
        catch{
            print(error)
        }
        return cards
    }
    


    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let set = NSEntityDescription.insertNewObject(forEntityName: "FlashCardEntity", into: cntxt()) as! FlashCardEntity
        count +=  1
        set.term = "Term \(count)"
        set.definition = "Definition \(count)"
        cards.append(set)
        do {
            try cntxt().save()
        }
        catch{
            print(error)
        }
        //self.updateData(fcs: cards)
        tableView.reloadData()
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)

    }
    
    
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
                return
            }

        let p = gestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: p) {
            createCustomAlert(card: cards[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cds = cards[indexPath.row]
        let alert = UIAlertController(title: cds.term, message: cds.definition, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.cards[indexPath.row])
            tableView.reloadData()
        }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        tableView.reloadData()
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(card: FlashCardEntity)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.card = card
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
        tableView.reloadData()    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
    

}
