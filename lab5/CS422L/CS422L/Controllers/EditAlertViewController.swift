//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit
import CoreData

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: FlashCardEntity = FlashCardEntity()
    var cards: [FlashCardEntity] = []
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func dataset() -> [FlashCardEntity]{
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FlashCardEntity")
        var data: [FlashCardEntity] = []
        do{
            data = try context.fetch(fetchRequest) as! [FlashCardEntity]
        }
        catch{
            print(error)
        }
        return data
    }
    
    func setup()
    {
        
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        
//        card.term?.removeAll()
//        card.definition?.removeAll()
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FlashCardEntity")
        var data: [FlashCardEntity] = []
        do{
            data = try context.fetch(fetchRequest) as! [FlashCardEntity]
        }
        catch{
            print(error)
        }
        if let index = parentVC?.dataset().firstIndex(of: card){
            data.remove(at: index)
        }
        if let index = parentVC?.cards.firstIndex(of: card){
            parentVC?.cards.remove(at: index)
        }
        do {
            try context.save()
        }
        catch{
            print(error)
        }
        parentVC?.tableView.reloadData()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            //do something / save the edits
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            self.card.term = self.termEditText.text
            self.card.definition = self.definitionEditText.text
            do {
                try context.save()
            }
            catch{
                print(error)
            }
            self.parentVC?.tableView.reloadData()
        })
        
    }
    
}
